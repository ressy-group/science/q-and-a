#!/usr/bin/env python
from Bio import AlignIO
from Bio.Phylo.TreeConstruction import DistanceCalculator, DistanceTreeConstructor
from Bio.Phylo.Consensus import *
from Bio.Phylo.Consensus import _count_clades

def bootstrap_consensus_fixed(alignment, times, tree_constructor, consensus):
    """Consensus tree of a series of bootstrap trees for a multiple sequence alignment.

    :Parameters:
        alignment : Alignment or MultipleSeqAlignment object
            Multiple sequence alignment to generate replicates.
        times : int
            Number of bootstrap times.
        tree_constructor : TreeConstructor
            Tree constructor to be used to build trees.
        consensus : function
            Consensus method in this module: ``strict_consensus``,
            ``majority_consensus``, ``adam_consensus``.

    """
    trees = bootstrap_trees_fixed(alignment, times, tree_constructor)
    tree = consensus(trees)
    return tree

def bootstrap_trees_fixed(alignment, times, tree_constructor):
    """Generate bootstrap replicate trees from a multiple sequence alignment.

    :Parameters:
        alignment : Alignment or MultipleSeqAlignment object
            multiple sequence alignment to generate replicates.
        times : int
            number of bootstrap times.
        tree_constructor : TreeConstructor
            tree constructor to be used to build trees.

    """
    if isinstance(alignment, MultipleSeqAlignment):
        length = len(alignment[0])
        for i in range(times):
            bootstrapped_alignment = None
            for j in range(length):
                col = random.randint(0, length - 1)
                if bootstrapped_alignment is None:
                    bootstrapped_alignment = alignment[:, col : col + 1]
                else:
                    bootstrapped_alignment += alignment[:, col : col + 1]
            tree = tree_constructor.build_tree(bootstrapped_alignment)
            yield tree
    else:
        n, m = alignment.shape
        for i in range(times):
            cols = [random.randint(0, m - 1) for j in range(m)]
            tree = tree_constructor.build_tree(alignment[:, cols])
            yield tree

def majority_consensus_fixed(trees, cutoff=0):
    """Search majority rule consensus tree from multiple trees.

    This is a extend majority rule method, which means the you can set any
    cutoff between 0 ~ 1 instead of 0.5. The default value of cutoff is 0 to
    create a relaxed binary consensus tree in any condition (as long as one of
    the provided trees is a binary tree). The branch length of each consensus
    clade in the result consensus tree is the average length of all counts for
    that clade.

    :Parameters:
        trees : iterable
            iterable of trees to produce consensus tree.

    """
    trees = list(trees)
    print(f"majority_consensus: {len(trees)} trees")
    tree_iter = iter(trees)
    first_tree = next(tree_iter)

    terms = first_tree.get_terminals()
    bitstr_counts, tree_count = _count_clades(itertools.chain([first_tree], tree_iter))
    print(f"tree_count: {tree_count}")
    print(f"terms ({len(terms)}): {terms}")
    print(f"bitstr_counts: {bitstr_counts}")

    # Sort bitstrs by descending #occurrences, then #tips, then tip order
    bitstrs = sorted(
        bitstr_counts.keys(),
        key=lambda bitstr: (bitstr_counts[bitstr][0], bitstr.count("1"), str(bitstr)),
        reverse=True,
    )
    print(f"bitstrs: {bitstrs}")
    root = BaseTree.Clade()
    if bitstrs[0].count("1") == len(terms):
        root.clades.extend(terms)
    else:
        raise ValueError("Taxons in provided trees should be consistent")

    # Make a bitstr-to-clades dict and store root clade
    bitstr_clades = {bitstrs[0]: root}
    # create inner clades
    for bitstr in bitstrs[1:]:
        # apply majority rule
        count_in_trees, branch_length_sum = bitstr_counts[bitstr]
        confidence = 100.0 * count_in_trees / tree_count
        print(f"bitstr {bitstr}: confidence  = 100 * count_in_trees / tree_count = 100 * {count_in_trees} / {tree_count} = {confidence}")
        if confidence < cutoff * 100.0:
            print(f"confidence < cutoff * 100 ({confidence} < {cutoff} * 100); breaking")
            break
        clade_terms = [terms[i] for i in bitstr.index_one()]
        clade = BaseTree.Clade()
        clade.clades.extend(clade_terms)
        clade.confidence = confidence
        clade.branch_length = branch_length_sum / count_in_trees
        bsckeys = sorted(bitstr_clades, key=lambda bs: bs.count("1"), reverse=True)

        # check if current clade is compatible with previous clades and
        # record its possible parent and child clades.
        compatible = True
        parent_bitstr = None
        child_bitstrs = []  # multiple independent childs
        for bs in bsckeys:
            if not bs.iscompatible(bitstr):
                compatible = False
                break
            # assign the closest ancestor as its parent
            # as bsckeys is sorted, it should be the last one
            if bs.contains(bitstr):
                parent_bitstr = bs
            # assign the closest descendant as its child
            # the largest and independent clades
            if (
                bitstr.contains(bs)
                and bs != bitstr
                and all(c.independent(bs) for c in child_bitstrs)
            ):
                child_bitstrs.append(bs)
        if not compatible:
            continue

        if parent_bitstr:
            # insert current clade; remove old bitstring
            parent_clade = bitstr_clades.pop(parent_bitstr)
            # update parent clade childs
            parent_clade.clades = [
                c for c in parent_clade.clades if c not in clade_terms
            ]
            # set current clade as child of parent_clade
            parent_clade.clades.append(clade)
            # update bitstring
            # parent = parent ^ bitstr
            # update clade
            bitstr_clades[parent_bitstr] = parent_clade

        if child_bitstrs:
            remove_list = []
            for c in child_bitstrs:
                remove_list.extend(c.index_one())
                child_clade = bitstr_clades[c]
                parent_clade.clades.remove(child_clade)
                clade.clades.append(child_clade)
            remove_terms = [terms[i] for i in remove_list]
            clade.clades = [c for c in clade.clades if c not in remove_terms]
        # put new clade
        bitstr_clades[bitstr] = clade
        if (len(bitstr_clades) == len(terms) - 1) or (
            len(bitstr_clades) == len(terms) - 2 and len(root.clades) == 3
        ):
            print(f"len(bitstr_clades) == len(terms) - 1  -->   {len(bitstr_clades)} ?= {len(terms)} - 1")
            print(f"len(bitstr_clades) == len(terms) - 2 and len(root.clades) == 3  -->   {len(bitstr_clades)} ?= {len(terms)} - 2 and {len(root.clades)} ?= 3")
            print("(one of those is True; break)")
            break
    return BaseTree.Tree(root=root)


out_fp = "tmp_out.fa"
out_fp = "test4.fa"

with open(out_fp,'r') as afa:
    alignment = AlignIO.read(afa, 'fasta')

calculator = DistanceCalculator('identity')
constructor = DistanceTreeConstructor(method='upgma', distance_calculator=calculator)

## get the list of bootstrap trees
#consensus_tree = bootstrap_consensus_fixed(alignment=alignment, times=50, tree_constructor=constructor, consensus=majority_consensus)

import Bio.Align
print(type(alignment))
print(dir(alignment))
print(list(alignment))
alignment_alt = Bio.Align.Alignment(list(alignment))
print(alignment_alt)

consensus_tree = bootstrap_consensus(alignment=alignment_alt, times=50, tree_constructor=constructor, consensus=majority_consensus)

# https://biopython.org/docs/latest/api/Bio.Align.html#Bio.Align.Alignment
# https://biopython.org/docs/latest/api/Bio.Align.html#Bio.Align.MultipleSeqAlignment

print("===")
print(consensus_tree)
