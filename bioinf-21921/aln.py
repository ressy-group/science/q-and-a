#!/usr/bin/env python
### alignment
from Bio.Align.Applications import MuscleCommandline
from Bio import AlignIO, SeqIO

# Note that as of 1.81 or so Biopython considers this feature obsolete and just
# says to roll your own process calls as needed
muscle_exe = "muscle"
muscle_cline = MuscleCommandline(muscle_exe, input="tmp_in.fa", out="tmp_out.fa")
muscle_cline()
# Also latest muscle doesn't even use these arguments.
# Should now be:
# muscle -align tmp_in.fa -output tmp_out.fa
