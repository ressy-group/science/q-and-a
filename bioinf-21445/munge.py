#!/usr/bin/env python

import sys
import hashlib
from Bio import SeqIO

def md5(txt):
    return hashlib.md5(str(txt).encode("UTF8")).hexdigest()

def load_cdr_fwr(path):
    rows = []
    with open(path) as f_in:
        for line in f_in:
            rows.append(line.strip().split())
    return rows

# We do need to at least have the locus (IG?V) noted in the Seq IDs, or it'll
# only blast V in the igblast output.  Other than that I don't think it
# matters.

cdrfwr = load_cdr_fwr("/home/jesse/miniconda3/envs/bioinf-21445/share/igblast/internal_data/rhesus_monkey/rhesus_monkey.ndm.imgt")
with open("cdrfwr.tab", "w") as f_out:
    for row in cdrfwr:
        newid = "IG" + row[0][1] + "V-" + md5(row[0])
        row[0] = newid
        f_out.write("\t".join(row) + "\n")

with open("dbv.fa", "w") as f_out:
    for rec in SeqIO.parse("orig_nt.fa", "fasta"):
        new_id = "IG" + rec.id[1] + "V-" + md5(rec.id)
        f_out.write(f">{new_id}\n{rec.seq}\n")
